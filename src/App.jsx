import React, { useEffect, useState } from 'react'

export default function App () {
  const [first, setfirst] = useState(0)
  const [theme ,setTheme ]=useState('green')

  function updater (e) {
    e.preventDefault()
    setfirst(cur => {
      return e.target.textContent === '+' ? cur + 1 : cur - 1
    })
  }

  return (
    <div>
      <button onClick={updater}>-</button>
      <span>{0 || first} {theme}</span>
      <button onClick={updater}>+</button>
    </div>
  )
}
